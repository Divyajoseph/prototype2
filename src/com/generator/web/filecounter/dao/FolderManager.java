package com.generator.web.filecounter.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FolderManager {
	
	public static void copyFolder(String srcPath, String destPath) throws IOException {
		File src = new File(srcPath);
		File dest = new File(destPath);
		copyFolder(src,dest);
	}
	public static void copyFolder(File src, File dest) throws IOException {
		
		  if (src.isDirectory()) {
		    //if directory not exists, create it
		    if (dest.exists()) {
		      dest.delete();
		    }
		    dest.mkdir();
		    //list all the directory contents
		    String files[] = src.list();
		    for (String file : files) {
		      //construct the src and dest file structure
		      File srcFile = new File(src, file);
		      File destFile = new File(dest+"\\"+file);
		      //recursive copy
		      copyFolder(srcFile,destFile);
		    }
		  } else {
		    //if file, then copy it
		    //Use bytes stream to support all file types
		    InputStream in = new FileInputStream(src);
		    OutputStream out = new FileOutputStream(dest); 
		    byte[] buffer = new byte[1024];
		    int length;
		    //copy the file content in bytes 
		    while ((length = in.read(buffer)) > 0){
		      out.write(buffer, 0, length);
		    }
		    in.close();
		    out.close();
		    System.out.println("File copied from " + src + " to " + dest);
		  }
		}

	public static void copyFolder(File fileToChange, Object attributes) throws IOException {
		
		}
	
	
	public static String getTemplateFolderPath(String templateId){
		return "C:\\EclipseJee\\FirstApp\\WebContent\\Templates\\".concat(templateId);
			}
	public static String getWorkingFolderPath(String userId){	
		String folderPath = "C://EclipseJee//.metadata//.plugins//org.eclipse.wst.server.core//tmp0//wtpwebapps//FirstApp//WorkingFolders//";
		return folderPath.concat(userId);
	}
	
	public static String getDeployedFolderPath(String userId){	
		String folderPath = "C://EclipseJee//.metadata//.plugins//org.eclipse.wst.server.core//tmp0//wtpwebapps//FirstApp//DeployedFolder//";
		return folderPath.concat(userId);
	}
	
}
	
