package com.generator.web.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.generator.web.filecounter.dao.FolderManager;

/**
 * Servlet implementation class FileCounter
 */

@WebServlet("/SiteModifier")
public class SiteModifier extends HttpServlet {
  private static final long serialVersionUID = 1L;


  @Override
  protected void doGet(HttpServletRequest request,
      HttpServletResponse response) throws ServletException, IOException {
	  HttpSession session = request.getSession(true);
		  
	  String userId = (String) session.getAttribute("userId");
	  if(userId==null)
	  {
		  userId = "divya";
	  }
	  String WorkingFolder = FolderManager.getWorkingFolderPath(userId); 
	  response.sendRedirect(WorkingFolder+"//index.html");	  
  }

  @Override
  protected void doPost(HttpServletRequest request,
      HttpServletResponse response) throws ServletException, IOException {
	
	  HttpSession session = request.getSession(true);
	 String userId = 	 (String) session.getAttribute("userId");	 
	 if(userId==null)
	  {
		  userId = "divya";
	  }
	 String WorkingFolder = FolderManager.getWorkingFolderPath(userId); 
	 FolderManager.copyFolder(WorkingFolder+"\\index.html", WorkingFolder+"\\final.html");
	 
	 FolderManager.copyFolder(WorkingFolder+"\\final.html", "AttributesList");
	 response.sendRedirect(WorkingFolder+"\\final.html");
  
  }
 
  
  
  @Override
  public void init() throws ServletException {
  
    try {
     
    } catch (Exception e) {
      getServletContext().log("An exception occurred ", e);
      throw new ServletException("An exception occurred"
          + e.getMessage());
    }
  }
  
  public void destroy() {
    super.destroy();
    try {
   
    	
    	
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

} 