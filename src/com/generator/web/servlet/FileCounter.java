package com.generator.web.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.generator.web.filecounter.dao.FileDao;

/**
 * Servlet implementation class FileCounter
 */

@WebServlet("/FileCounter")
public class FileCounter extends HttpServlet {
  private static final long serialVersionUID = 1L;

 
  private FileDao dao;

  @Override
  protected void doGet(HttpServletRequest request,
      HttpServletResponse response) throws ServletException, IOException {
    // Set a cookie for the user, so that the counter does not increate
    // every time the user press refresh
    HttpSession session = request.getSession(true);
    // Set the session valid for 5 secs
    session.setMaxInactiveInterval(5);
      
    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    out.println(
    		"<!DOCTYPE html> <html>   <head> <meta charset=\"ISO-8859-1\"> "+
"    <title>Insert title here</title> "+
"    </head>"+
"    <body> <form ACTION=\"../post2\" METHOD=\"POST\">"+
"    <div>"+
"    <a href=\"Home.html\">Home</a>"+
"    <br>"+
"    <input type=\"text\" id=\"userId\" name=\"userId\" > <br>"+
"    <input type=\"text\" id=\"templateId\"  name=\"templateId\" > <br>"+
 "<input name=\"send\"  type=\"submit\"  value=\"send\"/>" +
"    <br>"+
"    <a href=\"SiteModifier.html\">select template and click to  create/modify Site (Existing User)</a>"+
"    </div> </form>"+ 
 "</body>"+
    "</html>");
    
  
  }

  
  @Override
  public void init() throws ServletException {
    dao = new FileDao();
    try {
    } catch (Exception e) {
      getServletContext().log("An exception occurred in FileCounter", e);
      throw new ServletException("An exception occurred in FileCounter"
          + e.getMessage());
    }
  }
  
  public void destroy() {
    super.destroy();
    try {
      int count = 0;
	dao.save(count);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

} 