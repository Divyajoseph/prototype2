package com.generator.web.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

 
import com.generator.web.filecounter.dao.FolderManager;


/**
 * Servlet implementation class FileCounter
 */

@WebServlet("/TemplateSelector")
public class TemplateSelector extends HttpServlet {
  private static final long serialVersionUID = 1L;


  @Override
  protected void doGet(HttpServletRequest request,
      HttpServletResponse response) throws ServletException, IOException {
	  HttpSession session = request.getSession(true);
	    // Set the session valid for 5 secs
	    session.setMaxInactiveInterval(5);
	      
	    response.setContentType("text/html");
	    response.sendRedirect("TemplateSelector.html");
	   /* PrintWriter out = response.getWriter();
	    out.println(
	    		"<!DOCTYPE html> <html>   <head> <meta charset=\"ISO-8859-1\"> "+
	"    <title>Insert title here</title> "+
	"    </head>"+
	"    <body> <form ACTION=\"../TeamplateSelector\" METHOD=\"POST\">"+
	"    <div>"+
	"    <a href=\"Home.html\">Home</a>"+
	"    <br>"+
	"    <input type=\"text\" id=\"userId\" name=\"userId\" > <br>"+
	"    <input type=\"text\" id=\"templateId\"  name=\"templateId\" > <br>"+
	 "<input name=\"send\"  type=\"submit\"  value=\"send\"/>" +
	"    <br>"+
	"    <a href=\"SiteModifier.html\">select template and click to  create/modify Site (Existing User)</a>"+
	"    </div> </form>"+ 
	 "</body>"+
	    "</html>");*/
  }

  @Override
  protected void doPost(HttpServletRequest request,
      HttpServletResponse response) throws ServletException, IOException {	  
	 String userId = request.getParameter("userId");
	 String   templateId = request.getParameter("templateId");
	 String WorkingFolder = templateSelected(userId,templateId);
	 HttpSession session = request.getSession(true);
	 session.setAttribute("userId", userId);
	 session.setAttribute("templateId", templateId);
	 session.setAttribute("WorkingFolder", WorkingFolder);
	 response.sendRedirect(WorkingFolder.concat("\\index.html"));
  
  }
  
  private String  templateSelected(String userId, String templateId) throws IOException
  {
	  String destFolder = FolderManager.getWorkingFolderPath(userId);
	FolderManager.copyFolder(FolderManager.getTemplateFolderPath(templateId),
			FolderManager.getWorkingFolderPath(userId));
	return destFolder;
  }
  
  
  
  @Override
  public void init() throws ServletException {
  
    try {
     
    } catch (Exception e) {
      getServletContext().log("An exception occurred ", e);
      throw new ServletException("An exception occurred"
          + e.getMessage());
    }
  }
  
  public void destroy() {
    super.destroy();
    try {
   
    	
    	
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

} 