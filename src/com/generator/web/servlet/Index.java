package com.generator.web.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Index
 */

@WebServlet("/Index")
public class Index extends HttpServlet {
  private static final long serialVersionUID = 1L;


  @Override
  protected void doGet(HttpServletRequest request,
      HttpServletResponse response) throws ServletException, IOException {	  
	  response.sendRedirect("index.html");	  
  }

  @Override
  protected void doPost(HttpServletRequest request,
      HttpServletResponse response) throws ServletException, IOException {
	
	 String userId = request.getParameter("userId");		 
	 HttpSession session = request.getSession(true);
	 session.setAttribute("userId", userId);
	 response.sendRedirect("SiteModifier");
	 return;
  
  }
 
  
  
  @Override
  public void init() throws ServletException {
  
    try {
     
    } catch (Exception e) {
      getServletContext().log("An exception occurred ", e);
      throw new ServletException("An exception occurred"
          + e.getMessage());
    }
  }
  
  public void destroy() {
    super.destroy();
    try {
   
    	
    	
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

} 